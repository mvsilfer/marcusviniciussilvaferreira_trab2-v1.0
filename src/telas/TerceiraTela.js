import React from 'react';
import { useState } from 'react';

import {Button, StyleSheet, View, TextInput, Text} from 'react-native';

const TerceiraTela =({navigation, route})=>{
    const conta = route.params.conta;

    return(
        <View style={styles.main}>
            <Text style={{textAlign:'center',fontSize:41, color:'rgb(8,7,128)',paddingBottom:35}}>
                Configurações
            </Text>
            <View style={styles.buttons}>
                <Button
                    style={{flex:1}}
                    title="Home"
                    onPress={()=>{navigation.popToTop()}}
                />
            </View>
            <View style={styles.buttons}>
                <Button
                    style={{flex:1}}
                    title='Cadastro'
                    onPress={()=>{navigation.navigate("config_to_cadastro",
                    {
                        conta:{
                            nome:conta.nome,
                            email:conta.email,
                            telefone:conta.telefone,
                            isFeminino:conta.isFeminino,
                        }
                    }
                    )}}
                />
            </View>
        </View>
        
    )
};

const styles  = StyleSheet.create({
    main:{
        flex:1,
        margin:10,
        flexDirection:'column',
        backgroundColor:'rgb(44,145,210)',
    },
    buttons:{
        marginBottom:45,
        marginTop:45,
        flex:1,

    },
});

export default TerceiraTela;