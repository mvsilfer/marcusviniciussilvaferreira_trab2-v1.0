import React from 'react';
import { useState } from 'react';

import {Button, StyleSheet, View, TextInput, Text} from 'react-native';



const SegundaTela =({navigation, route})=>{
    console.log(route);
    const conta = route.params.conta;
    return(
        <View style={styles.mainView}>    
            <Text style={styles.title}>Cadastro</Text> 
            <View style={styles.viewsDados}>
                <Text style={styles.dadosTitle}>Nome:</Text>    
                <Text>{conta.nome}</Text>
            </View>
            <View style={styles.viewsDados}>
                <Text style={styles.dadosTitle}>Email:</Text>    
                <Text>{conta.email}</Text>
            </View>
            <View style={styles.viewsDados}>
                <Text style={styles.dadosTitle}>Telefone:</Text>    
                <Text>{conta.telefone}</Text>
            </View>
            <View style={styles.viewsDados}>
                <Text style={styles.dadosTitle}>Sexo:</Text>    
                {conta.isFeminino?<Text>Feminino</Text>:<Text>Masculino</Text>}
            </View>
            <View style={styles.viewsDados}>
                <Button
                    title="Home"
                    onPress={()=>{navigation.popToTop()}}
                />
            </View>
            <View style={styles.viewsDados}>
                <Button
                    title="Configurações"
                    onPress={()=>{navigation.navigate("cadastro_to_config",
                    {
                        conta:{
                           nome:conta.nome,
                           email:conta.email,
                           telefone:conta.telefone,     
                        }
                    }
                    )}}
                />
            </View>
        </View>
    )
    
} ;

const styles = StyleSheet.create({
    secondaryView:{
        marginLeft:15,
    },
    mainView:{
        flex:1,
        alignItems:'center',
        paddingTop:15,
        marginLeft:15,
    },
    title:{
        fontStyle:'italic',
        fontSize: 28,
    },
    viewsDados:{
        paddingTop:40, 
        fontSize:30,
    },
    dadosTitle:{
        textAlign:'center',
        fontSize:25,
    },
    dados:{
        alignItems:'flex-start',
        flex:1,
        margin:15
    },
});
export default SegundaTela;