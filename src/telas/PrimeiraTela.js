import React from 'react';
import { useState } from 'react';

import {Button, StyleSheet, View, TextInput, Switch, Text} from 'react-native';



const PrimeiraTela =({navigation, route})=>{
    const [nome, setNome] = useState(''); 
    const [email, setEmail] = useState('');
    const [telefone, setTelefone] = useState('');
    const [isFeminino,setIsFeminino] = useState(false);
    const toggleSwitchFemininoAtivo = () => setIsFeminino(previousState => ! previousState);

    return(
        <View style={styles.mainView}>
            <Text style={styles.title}>Criar Conta: </Text>
            <View style={styles.viewsentradas}>
                <Text style={{textAlign:'center', fontSize:25,}}>Nome:</Text>
                <TextInput
                    keyboardType ='default'
                    style={styles.entradas}
                    placeholder="Nome..."
                    onChangeText = {nome => setNome(nome)}
                    value={nome}
                />
            </View>
            <View style={styles.viewsentradas}>
                <Text style={{textAlign:'center', fontSize:25}}>Email:</Text>
                <TextInput
                    keyboardType='email-address'
                    style={styles.entradas}
                    placeholder="Email..."
                    onChangeText = {email => setEmail(email)}
                    value={email}
                />
            </View>     
            <View style={styles.viewsentradas}>
                <Text style={{textAlign:'center', fontSize:25}}>Telefone:</Text>
                <TextInput
                    keyboardType='phone-pad'
                    style={styles.entradas}
                    maxLength= {15}
                    placeholder="Telefone..."
                    onChangeText = {telefone => setTelefone(telefone)}
                    value={telefone}
                />
            </View>
            <View style={styles.viewsentradas}>
                <View style={{flexDirection:'row'}}>
                    <Text style={{fontSize:20}}>Masculino</Text>
                    <Switch
                        style={{marginLeft:10, marginRight:10}}
                        trackColor={{false:'blue', true:"#81b0ff"}}
                        thumbColor={isFeminino?"blue":"#81b0ff"}
                        onValueChange={toggleSwitchFemininoAtivo}
                        value={isFeminino}
                    />
                    <Text style={{fontSize:20}}>Feminino</Text>
                    
                </View>
            </View>
            <View style={styles.viewsentradas}>
                <Button
                    title='Cadastro'
                    onPress={()=>{navigation.navigate("home_to_cadastro",
                        {
                            conta:{
                                nome:nome, 
                                email:email, 
                                telefone:telefone,
                                isFeminino:isFeminino,
                            }
                        }
                    )}}
                />
            </View>  
            <View style={styles.viewsentradas}>
                <Button
                    title='Configurações'
                    onPress={()=>{navigation.navigate("home_to_config",
                        {
                            conta:{
                                nome:nome, 
                                email:email, 
                                telefone:telefone,
                                isFeminino:isFeminino,
                            }    
                        }
                    )}}
                />
            </View> 
        </View>    
    )
    
} ;

const styles = StyleSheet.create({
    mainView:{
        flex:1,
        alignItems: 'center',
        paddingTop:15,
        backgroundColor:'rgb(44,145,210)',
    },
    title:{
        fontStyle:'italic',
        fontSize: 18,
    },
    entradas:{
        height:40, 
        width:280, 
        borderColor:'gray',
        padding:5, 
        borderWidth: 1,
    },
    viewsentradas:{
        paddingTop:40, 
        fontSize:17,
    },
});
export default PrimeiraTela;