import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import PrimeiraTela from './src/telas/PrimeiraTela';
import SegundaTela from './src/telas/SegundaTela';
import TerceiraTela from './src/telas/TerceiraTela';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName = 'App_to_home'>
          <Stack.Screen
            name='App_to_home'
            component={PrimeiraTela}
          />
          <Stack.Screen
            name='home_to_cadastro'
            component={SegundaTela}
          />
          <Stack.Screen
            name='cadastro_to_config'
            component={TerceiraTela}
          />
          <Stack.Screen
            name='home_to_config'
            component={TerceiraTela}
          />
          <Stack.Screen
            name='config_to_cadastro'
            component={SegundaTela}
          />
        </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
